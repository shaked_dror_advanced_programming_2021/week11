#include "threads.h"

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);

	t.join();
}

void printVector(std::vector<int> primes)
{
	int i = 0;
	for (i = 0; i < primes.size(); i++)
	{
		std::cout << primes.at(i) <<std::endl;
	}
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	for (int i = begin; i <= end; i++)
	{
		//if the number is prime number will put the number in the vector
		if (check_if_prime_number(i))
		{
			primes.push_back(i);
		}
	}
}

std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;
	clock_t time = clock();//will save the start time

	std::thread t(getPrimes, begin, end, std::ref(primes));
	t.join();

	float the_time = clock() - time;//will save the end time
	std::cout << "the times is: " << the_time / CLOCKS_PER_SEC;//will print the time

	return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	for (int i = begin; i <= end; i++)
	{
		//if the number is prime number will put the number in the file
		if (check_if_prime_number(i))
		{
			file<< i << std::endl;
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file (filePath);
	
	if (file.is_open())
	{
		clock_t time = clock();//will save the start time
		int the_move = (end - begin) / N;
		int temp_begin = 0;
		int temp_end = 0;
		for (int i = begin; i <= end; i+= the_move)
		{
			temp_begin = i;	
			temp_end = i + the_move;
			if (temp_end > end)//if the temp_end is bigger than the move the temp_end will be the end
			{
				temp_end = end;
			}

			std::thread t(writePrimesToFile, temp_begin, temp_end, std::ref(file));
			t.join();
		}
		float the_time = clock() - time;//will save the end time
		std::cout << "the times is: " << the_time / CLOCKS_PER_SEC << std::endl;//will print the time
		file.close();
	}
	else
	{
		std::cout << "file error" << std::endl;
	}
}

//Will get a number and check if it is a prime number
bool check_if_prime_number(int num)
{
	for (int i = 2; i < num; i++)
	{
		if (num % i == 0)//Checks if it is divisible
		{
			return false;
		}
	}
	return true;
}
